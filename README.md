A NodeJS application that finds domains that are available comtaining variants of the keywords provided

For support please contact sales at [Potentiate](http://potentiate.com)

## Install

```
$ npm install domain-finder
```

Usage
=====

```js
import {EDCClient, EDCClientConfiguration} from 'informatica';
// or:
var edc_client = require("informatica").EDCClient;
```

To create a data domain group

```js
var edc_client_config = {
    hostname: 'myhostname.com', //Replace with the provided hostname. Do not add protocol (http or https)
    port: 8085, //Default is 8085
    username: 'my_username', //Replace with the provided username
    password: 'my_password' //Replace with the provided password
};

edc_client.createDataDomainGroup(edc_client_config, name, description).then(function () {
    //Do something here
}, function (error) {
 console.error('Error creating data domain group [' + object.Key + ']. Error [' + error + '].');
});
```